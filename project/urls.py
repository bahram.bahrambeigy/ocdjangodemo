from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from DemoApp.views import IndexView

urlpatterns = [
    url(r'^$', IndexView.as_view(), name='index-view'),
    url(r'^admin/', include(admin.site.urls)),
]
